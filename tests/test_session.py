"""get_session tests."""
import contextlib
import os
import tempfile
import unittest
from unittest import mock

from cki_lib.session import get_session


class TestSession(unittest.TestCase):
    """Test get_session."""

    @staticmethod
    @contextlib.contextmanager
    def _bundles(requests='', curl='', fedora='', debian=''):
        with mock.patch.multiple('cki_lib.session',
                                 FEDORA_BUNDLE=fedora, DEBIAN_BUNDLE=debian), \
                mock.patch.dict(os.environ, {
                    'REQUESTS_CA_BUNDLE': requests, 'CURL_CA_BUNDLE': curl}), \
                tempfile.NamedTemporaryFile() as file:
            yield file.name

    def test_bundle_none(self):
        """Test the verify field with no bundle overrides."""
        with self._bundles():
            session = get_session('agent')
        self.assertEqual(session.verify, True)

    def test_bundle_fedora(self):
        """Test the verify field with an existing Fedora bundle."""
        with self._bundles() as name, \
                mock.patch('cki_lib.session.FEDORA_BUNDLE', name):
            session = get_session('agent')
        self.assertEqual(session.verify, name)

    def test_bundle_fedora_missing(self):
        """Test the verify field with a missing Fedora bundle."""
        with self._bundles(), \
                mock.patch('cki_lib.session.FEDORA_BUNDLE', 'missing'):
            session = get_session('agent')
        self.assertEqual(session.verify, True)

    def test_bundle_debian(self):
        """Test the verify field with an existing Debian bundle."""
        with self._bundles() as name, \
                mock.patch('cki_lib.session.DEBIAN_BUNDLE', name):
            session = get_session('agent')
        self.assertEqual(session.verify, name)

    def test_bundle_debian_missing(self):
        """Test the verify field with a missing Debian bundle."""
        with self._bundles(), \
                mock.patch('cki_lib.session.DEBIAN_BUNDLE', 'missing'):
            session = get_session('agent')
        self.assertEqual(session.verify, True)

    def test_bundle_requests(self):
        """Test the verify field with an existing REQUESTS_CA_BUNDLE."""
        with self._bundles() as name, \
                mock.patch.dict(os.environ, {'REQUESTS_CA_BUNDLE': name}):
            session = get_session('agent')
        self.assertEqual(session.verify, name)

    def test_bundle_requests_missing(self):
        """Test the verify field with a missing REQUESTS_CA_BUNDLE."""
        with self._bundles(), \
                mock.patch.dict(os.environ, {'REQUESTS_CA_BUNDLE': 'missing'}):
            session = get_session('agent')
        self.assertEqual(session.verify, 'missing')

    def test_bundle_requests_preferred(self):
        """Test that REQUESTS_CA_BUNDLE is preferred over system bundles."""
        with self._bundles() as name, \
                mock.patch('cki_lib.session.FEDORA_BUNDLE', name),\
                mock.patch.dict(os.environ, {'REQUESTS_CA_BUNDLE': 'missing'}):
            session = get_session('agent')
        self.assertEqual(session.verify, 'missing')

    def test_bundle_curl_preferred(self):
        """Test that CURL_CA_BUNDLE is preferred over system bundles."""
        with self._bundles() as name, \
                mock.patch('cki_lib.session.FEDORA_BUNDLE', name),\
                mock.patch.dict(os.environ, {'CURL_CA_BUNDLE': 'missing'}):
            session = get_session('agent')
        self.assertEqual(session.verify, 'missing')
