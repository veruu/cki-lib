"""Tests for cki_pipeline."""
# pylint: disable=protected-access
import os
import unittest
from unittest import mock

from cki_lib import cki_pipeline
from cki_lib import misc
from tests import fakes


@mock.patch('time.sleep', mock.Mock())
class TestTrigger(unittest.TestCase):
    """Tests for cki_pipeline.trigger_multiple()."""

    required_variables = {'cki_project': 'cki-project',
                          'cki_pipeline_branch': 'test_branch',
                          'cki_pipeline_type': 'baseline',
                          'title': 'title'}
    required_variables_env = {'cki_pipeline_project': 'cki-pipeline',
                              'cki_pipeline_branch': 'test_branch',
                              'cki_pipeline_type': 'baseline',
                              'title': 'title'}
    multiple_variables = [{
        'cki_project': 'cki-project',
        'cki_pipeline_branch': 'branch 1',
        'title': 'title 1',
        'cki_pipeline_type': 'baseline',
    }, {
        'cki_project': 'cki-project',
        'cki_pipeline_branch': 'branch 2',
        'title': 'title 2',
        # 'cki_pipeline_type' missing
    }, {
        'cki_project': 'cki-project',
        'cki_pipeline_branch': 'branch 3',
        'title': 'title 3',
        'cki_pipeline_type': 'baseline',
    }]

    def _trigger_single(self, variables, project_name,
                        trigger_token='token'):
        gitlab = fakes.FakeGitLab()
        project = gitlab.add_project(project_name)
        project.branches.add_new_branch(variables['cki_pipeline_branch'])

        cki_pipeline.trigger_multiple(gitlab, [variables], trigger_token=trigger_token)

        pipeline = gitlab.projects[project_name].pipelines[0]
        for key, value in variables.items():
            if key == 'cki_pipeline_project':
                continue
            self.assertEqual(pipeline.attributes[key], value)
        self.assertEqual(pipeline.attributes['cki_project'], project_name)

    def test_trigger_single(self):
        """
        Test triggering a single pipeline. Only add required variables to make
        sure the code doesn't suddenly change to require something else.
        """
        self._trigger_single(self.required_variables, 'cki-project')

    @mock.patch.dict(os.environ, {'GITLAB_PARENT_PROJECT': 'cki-project'})
    def test_trigger_single_env(self):
        """
        Test triggering a single pipeline. Only add required variables to make
        sure the code doesn't suddenly change to require something else.
        """
        self._trigger_single(self.required_variables_env,
                             'cki-project/cki-pipeline')

    def test_trigger_single_api(self):
        """Test triggering a single pipeline via an API token."""
        self._trigger_single(self.required_variables, 'cki-project',
                             trigger_token=None)

    def _check_required_variables(self, variables, project_name, errors):
        gitlab = fakes.FakeGitLab()
        project = gitlab.add_project(project_name)
        project.branches.add_new_branch(variables['cki_pipeline_branch'])

        for expected in variables:
            missing = variables.copy()
            del missing[expected]
            with self.assertRaises(errors):
                cki_pipeline.trigger_multiple(gitlab, [missing],
                                              trigger_token='token')

    def test_required_variables(self):
        """
        Verify the expected variables are still required to trigger pipelines
        successfully.
        """
        self._check_required_variables(self.required_variables,
                                       'cki-project',
                                       (KeyError, misc.EnvVarNotSetError))

    @mock.patch.dict(os.environ, {'GITLAB_PARENT_PROJECT': 'cki-project'})
    def test_required_variables_env(self):
        """
        Verify the expected variables are still required to trigger pipelines
        successfully.
        """
        self._check_required_variables(self.required_variables_env,
                                       'cki-project/cki-pipeline',
                                       KeyError)

    def test_trigger_multiple_errors(self):
        """
        Test triggering multiple pipelines, some of which invalid. All
        pipelines should be tried.
        """
        variables = self.multiple_variables

        gitlab = fakes.FakeGitLab()
        project = gitlab.add_project('cki-project')
        for pipeline_variables in self.multiple_variables:
            project.branches.add_new_branch(pipeline_variables['cki_pipeline_branch'])

        with self.assertRaises(Exception):
            cki_pipeline.trigger_multiple(gitlab, variables,
                                          trigger_token='token')

        pipelines = gitlab.projects['cki-project'].pipelines.list()
        self.assertEqual(len(pipelines), 2)
        self.assertEqual(pipelines[0].attributes['title'],
                         variables[0]['title'])
        self.assertEqual(pipelines[1].attributes['title'],
                         variables[2]['title'])

    def test_returned_no_errors(self):
        """
        Verify the function returns a list of triggered pipelines if no errors
        were encountered when triggering.
        """
        gitlab = fakes.FakeGitLab()
        project = gitlab.add_project('cki-project')
        for pipeline_variables in self.multiple_variables:
            project.branches.add_new_branch(pipeline_variables['cki_pipeline_branch'])

        triggers = self.multiple_variables.copy()
        del triggers[1]  # Remove the trigger that's causing exception

        triggered_pipelines = cki_pipeline.trigger_multiple(gitlab, triggers,
                                                            trigger_token='token')
        self.assertEqual(len(triggered_pipelines), 2)


@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
class TestTriggerProduction(TestTrigger):
    """Tests for cki_pipeline.trigger_multiple() with IS_PRODUCTION=true."""
    required_variables = TestTrigger.required_variables.copy()
    required_variables.update({'title': 'commit_title'})
    required_variables_env = TestTrigger.required_variables_env.copy()
    required_variables_env.update({'title': 'commit_title'})


class TestCreateCommit(unittest.TestCase):
    """Test cases for utils.create_commit()."""

    def test_created_data(self):
        """Verify the content of created commit looks as expected."""

        commit_text = 'title\n\ncki_pipeline_branch = branch\ntitle = title'
        expected_data = {'branch': 'branch',
                         'actions': [],
                         'commit_message': commit_text}

        project = fakes.FakeGitLabProject()
        project.branches.add_new_branch('branch')
        cki_pipeline._create_commit(project,
                                    {'cki_pipeline_branch': 'branch',
                                     'title': 'title'})

        self.assertEqual(project.commits[0], expected_data)


class TestLastPipeline(unittest.TestCase):
    """
    Test cases for cki_pipeline._last_pipeline_for_branch() and
    cki_pipeline.last_successful_pipeline_for_branch().
    """

    def test_last_pipeline(self):
        """Verify the correct pipelines are returned with different filters."""

        project = fakes.FakeGitLabProject()

        branch = 'branch'
        cki_pipeline_type = 'baseline'
        scope = 'finished'
        variables = {'cki_pipeline_type': cki_pipeline_type, 'scope': scope}

        project.pipelines.add_new_pipeline(branch, 'token',
                                           variables, 'success')
        last_successful_pipeline = project.pipelines.list(ref=branch,
                                                          status='success')[0]

        project.pipelines.add_new_pipeline(branch, 'token',
                                           variables, 'failed')
        last_pipeline = project.pipelines.list(ref=branch,
                                               status='failed')[0]

        self.assertEqual(cki_pipeline._last_pipeline_for_branch(
            project, branch,
            variable_filter={'cki_pipeline_type': cki_pipeline_type},
            list_filter={"scope": scope}
        ), last_pipeline.id)
        self.assertEqual(cki_pipeline.last_successful_pipeline_for_branch(
            project, branch,
            variable_filter={'cki_pipeline_type': cki_pipeline_type},
        ), last_successful_pipeline.id)
