#!/bin/bash

# Write a stylized header, but fall back to plain text
# Args:
#   $@: Message to print
function cki_say {
    echo "$@" | toilet -f smmono12 -w 300 | lolcat -f || echo "$@"
}

# Write a bold green message (that looks like gitlab-runner's messages).
# Args:
#   $*: Message to print in green
function cki_echo_green {
    echo -e "\e[1;32m$*\e[0m"
}

# Write a bold red message (that looks like gitlab-runner's messages).
# Args:
#   $*: Message to print in red
function cki_echo_red {
    echo -e "\e[1;31m$*\e[0m"
}

# Write a bold yellow message (that looks like gitlab-runner's messages).
# Args:
#   $*: Message to print in red
function cki_echo_yellow {
    echo -e "\e[1;33m$*\e[0m"
}

# Parse a deployment-all-style bucket specification.
# Args:
#   $1: NAME of the env variable containing the bucket spec, eg.
#       "http://localhost:9000|cki_temporary|super_secret|software|subpath/"
# Exported variables:
#   AWS_ENDPOINT
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_BUCKET
#   AWS_BUCKET_PATH
function cki_parse_bucket_spec {
    IFS='|' read -r AWS_ENDPOINT AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_BUCKET AWS_BUCKET_PATH <<< "${!1}"
    export AWS_ENDPOINT AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_BUCKET AWS_BUCKET_PATH
}

# Call openssl enc with reasonable defaults
# Args:
#   $1: NAME of the env variable containing the password
#   $@: additional arguments for openssl enc
# Examples:
# - encrypt and save as base64: echo "secret" | cki_openssl_enc PASSWORD -e -a
# - decrypt again: echo "$ENCRYPTED_BASE64" | cki_openssl_enc PASSWORD -d -a
function cki_openssl_enc() {
    # OpenSSL parameters extracted from https://askubuntu.com/a/1126882
    openssl enc \
        -aes-256-cbc \
        -md sha512 \
        -pbkdf2 \
        -iter 100000 \
        -salt \
        -pass "env:$1" \
        "${@:2}"
}
