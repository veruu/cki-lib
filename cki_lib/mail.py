"""Email related helper functions."""

from datetime import date
from datetime import datetime
import glob
import operator
import os

from rcdefinition import const

from cki_lib.mailarchive_crawler import MailArchiveCrawler
from cki_lib.misc import create_mbox_at
from cki_lib.pipeline_message import PipelineMsgBase


def get_pipeline_messages(mailarchive_url, crafter_func, datespec, comp_op,
                          username=None):
    """Create an mbox with pipeline messages.

    Args:
        mailarchive_url: str, link to a specific mailing list archives
        crafter_func: callable that creates/filters messages, e.g.
                      PipelineMsgBase.from_file
        datespec: date to compare against using operator op, has to be within
                  current month's timeframe
        comp_op: operator to use to compare message datetime.date and datespec
        username: username to used to access the mailarchives, None for public
    """
    mhlp = MailArchiveCrawler(mailarchive_url, username, const.MAIL_COOKIE_LOC)

    mhlp.update_listarchive(const.MAIL_CACHE_PATH, delete_latest=False,
                            updatelastmonth=True)

    fpath = glob.glob(const.MAIL_CACHE_PATH + '/*')[0]
    messages = crafter_func(fpath)

    # remove the existing mbox and create a new empty mbox at its place
    os.remove(fpath)
    new_mbox = create_mbox_at(const.MAIL_CACHE_PATH, os.path.basename(fpath))
    for msg_obj in messages:
        if comp_op(datetime.date(msg_obj.tdelivered), datespec):
            new_mbox.add(msg_obj.msg)

    return fpath


def get_todays_skt_results_email_path():
    """Download today's skt-results-master mail, print path to mbox file."""
    fpath = get_pipeline_messages(const.MAILARCHIVE_SKT_RESULTS,
                                  PipelineMsgBase.from_file, date.today(),
                                  operator.eq)
    print(fpath)
