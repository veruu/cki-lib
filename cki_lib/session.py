"""Helper for creating requests Session."""
import os.path

import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

FEDORA_BUNDLE = '/etc/pki/tls/certs/ca-bundle.crt'
DEBIAN_BUNDLE = '/etc/ssl/certs/ca-certificates.crt'


def get_session(user_agent, logger=None):
    """Return pre-configured requests Session.

    The session contains configured user_agent, retries
    and optional logging.
    """
    session = requests.Session()

    session.headers.update({'User-Agent': user_agent})
    if logger:
        def log_request(response, *args, **kwargs):
            # pylint: disable=unused-argument
            logger.debug('Requested: %s', response.url)

        session.hooks = {'response': log_request}

    retry = Retry(total=5, backoff_factor=1)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)

    # By default, requests uses certificates from REQUESTS_CA_BUNDLE,
    # CURL_CA_BUNDLE, or the certifi package. Distributions divert the certifi
    # default to
    #   Fedora: /etc/pki/tls/certs/ca-bundle.crt
    #   Debian: /etc/ssl/certs/ca-certificates.crt
    # With a pip-installed requests, this is not the case. In that case, any
    # certs added to the system certificate bundle are ignored unless
    # REQUESTS_CA_BUNDLE is explicitly set. Do The Right Thing in this case and
    # use any of the distribution bundles if found.

    bundle = (os.environ.get('REQUESTS_CA_BUNDLE') or
              os.environ.get('CURL_CA_BUNDLE') or
              (os.path.isfile(FEDORA_BUNDLE) and FEDORA_BUNDLE) or
              (os.path.isfile(DEBIAN_BUNDLE) and DEBIAN_BUNDLE))
    if bundle:
        session.verify = bundle

    return session
