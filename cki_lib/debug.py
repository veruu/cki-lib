"""Debugging utility functions."""

from inspect import getframeinfo
from inspect import stack


def mark():
    """On call, print a marker/tracepoint."""
    try:
        mark.i += 1
    except AttributeError:
        mark.i = 1
    caller = getframeinfo(stack()[1][0])
    print(f'marker {mark.i} at {caller.filename}:{caller.lineno}')
